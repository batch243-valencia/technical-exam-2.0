let collection = [];

// Write the queue functions below.
// 1
function print() {
    return collection
}
// 2
function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
       collection[collection.length] = element;
    //    collection=[...collection, element]
       return collection
    }
// 3
function dequeue() {
    // In here you are going to remove the last element in the array
    // for (let i = 1; i < collection.length-1; i++) {
    //     collection[i] = collection[i+1];
    // }

    // 
    // let temp = [];
    // if (collection.length !== 0) {
    //     for (let i = 1; i < collection.length; i++) {
    //         temp[i - 1] = collection[i];
    //         }
    //     }
    // collection = temp;

    // try different approach


    // let temp = new Set();
    // if (collection.length !== 0) {
    //     for (let i = 1; i < collection.length; i++) {
    //         temp.add(collection[i])
    //         }
    //     }
    // collection =[...temp]

    if (collection.length !== 0) {
        for (let i = 1; i < collection.length; i++) {
            collection[i - 1] = collection[i];
            }
        }
    collection.length-=1
  return collection;
}
// 4
function front() {
    // In here, you are going to remove the first element
    return collection[0]
}
// 5
// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
    let int=0
    for (let i = 0; i < collection.length; i++) {
        int++
    }

    return int
}
// 6
function isEmpty() {
    return (!collection)?true:false;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};